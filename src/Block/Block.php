<?php

namespace Perles\Block;

abstract class Block
{
    public $controller;

    public $config;

    public $session;

    public $viewPath = BASE_DIR . 'views/';

    public $template;

    protected $data = [];

    public function __construct(\Perles\Controller\Controller $controller)
    {
        $this->controller = $controller;
        $this->config = new \Perles\Model\Config;
        $this->session = new \Perles\Model\Session;

        // Data has to be fetched before beforeRender call
        // To prevent headers already sent and have access to data
        $this->data = $this->data();

        $this->beforeRender();
        $this->twig = new \Perles\Model\Twig($this, $this->data);
        $this->afterRender();
    }

    protected function data()
    {
        return [];
    }

    protected function beforeRender()
    {
    }

    protected function afterRender()
    {
    }
}
