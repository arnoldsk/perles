<?php

namespace Perles\Block;

class Login extends Block
{
    public $template = 'auth/login.twig';

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
