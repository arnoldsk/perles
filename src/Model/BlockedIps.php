<?php

namespace Perles\Model;

class BlockedIps extends Model
{
    public $table = 'blocked_ips';

    public $identifier = 'ip';

    public $fillable = [
        'ip', 'created',
    ];
}
