<?php

namespace Perles\Model;

abstract class Model extends Database
{
    public $table;

    public $identifier = 'id';

    public $fillable = ['created'];

    public function collection()
    {
        $collectionClass = str_replace('Perles\Model', 'Perles\Model\Collection', get_class($this));
        if (!class_exists($collectionClass)) {
            $collectionClass = '\Perles\Model\Collection\Blank';
        }

        return new $collectionClass($this);
    }

    public function load($value, $field = null)
    {
        if (!$field) {
            $field = $this->identifier;
        }

        return $this->collection()->where($field, $value)->first();
    }

    public function delete($id, $field = null)
    {
        if (!$field) {
            $field = $this->identifier;
        }

        $this->beforeDelete($id);
        $query = "DELETE FROM `{$this->table}` WHERE `{$field}` = ?";
        $stmt = $this->prepare($query);
        $stmt->execute([$id]);
        $this->afterDelete($id);

        return $stmt != false;
    }

    protected function beforeDelete($id)
    {
    }

    protected function afterDelete($id)
    {
    }

    public function update($id, array $data)
    {
        $item = $this->load($id);
        $pieces = [];
        $bindables = [];

        if (!$item) {
            return false;
        }

        foreach ($item as $field => $value) {
            if (array_key_exists($field, $data)) {
                if ($field == 'created' && strtolower($data[$field]) == 'now()') {
                    $pieces[] = "`created` = NOW()";
                } else {
                    $pieces[] = "`{$field}` = ?";
                    $bindables[] = $data[$field];
                }
            }
        }

        $fields = implode(', ', $pieces);

        $query = "UPDATE `{$this->table}` SET {$fields} WHERE `{$this->identifier}` = ?";
        $bindables[] = $item->{$this->identifier};
        $stmt = $this->prepare($query);
        $stmt->execute($bindables);

        return $stmt != false;
    }

    public function createOrUpdate($field, array $data)
    {
        if (!isset($data[$field])) {
            throw new \Exception('createOrUpdate requires the data to contain a value for given field parameter, for example createOrUpdate(post_id, ["post_id" => 2]).');
        }

        $item = $this->load($data[$field], $field);

        if (!$item) {
            return $this->create($data);
        } else {
            return $this->update($data[$field], $data);
        }

        return false;
    }

    public function create($data)
    {
        // Allow altering the data before it's saved
        $this->beforeCreate($data);

        $piecies = [];
        $bindables = [];
        foreach ($data as $field => $value) {
            if (!in_array($field, $this->fillable)) {
                unset($data[$field]);
                continue;
            }

            if ($field == 'created' && strtolower($value) == 'now()') {
                $pieces[] = "`created` = NOW()";
            } else {
                $pieces[] = "`{$field}` = ?";
                $bindables[] = $value;
            }
        }

        $fields = implode(', ', $pieces);
        $query = "INSERT INTO `{$this->table}` SET {$fields}";
        $stmt = $this->prepare($query);
        $stmt->execute($bindables);

        $insertId = $stmt ? $this->lastInsertId() : null;

        $this->afterCreate($data, $insertId);

        return $insertId ?: false;
    }

    protected function beforeCreate(&$data)
    {
    }

    protected function afterCreate($data, $id)
    {
    }
}
