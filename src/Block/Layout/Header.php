<?php

namespace Perles\Block\Layout;

class Header extends \Perles\Block\Block
{
    public $template = 'layout/header.twig';

    protected function data()
    {
        return [
            'posts' => new \Perles\Model\Posts,
        ];
    }
}
