<?php

namespace Perles\Model\Collection;

use Perles\Model\PostsNicks;
use Perles\Model\Users;

class Posts extends Collection
{
    protected function afterGetRows(array &$rows)
    {
        foreach ($rows as $post) {
            if (isset($post->type) && $post->type == 'video') {
                preg_match('/youtube\.com\/((v|embed)\/)?[a-zA-Z0-9]+\/(.*)/', $post->url, $matches);
                $post->videoIdentifier = $matches[3];
            }

            if (isset($post->description)) {
                $post->descriptionRaw = $post->description;
                $post->description = $this->model->formatDescription($post->description);
            }

            $nicks = (new PostsNicks)->load($post->id, 'post_id');
            $post->nicks = $nicks ? $nicks->nicks : '';

            if ($post->user_id >= 0) {
                $users = new Users;
                $post->user = $users->load($post->user_id);
            } else {
                $post->user = null;
            }
        }
    }
}
