<?php

namespace Perles;

define('DS', DIRECTORY_SEPARATOR);
define('BASE_DIR', __DIR__ . DS . '..' . DS);
define('PUBLIC_DIR', BASE_DIR . 'public' . DS);

class Core
{
    protected $config;

    public function __construct()
    {
        // PHP 7 required for all plugins
        if (version_compare(PHP_VERSION, '7.0.0', '<')) {
            throw new \Exception('PHP version 7 required!');
        }

        $this->config = new \Perles\Model\Config;

        // Consistent timezone
        date_default_timezone_set($this->config->get('timezone'));
    }

    public function run()
    {
        $routes = require BASE_DIR . 'routes.php';
        new \Perles\Controller\Router($routes);
    }
}
