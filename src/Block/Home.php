<?php

namespace Perles\Block;

class Home extends Block
{
    public $template = 'home.twig';

    protected function data()
    {
        $posts = new \Perles\Model\Posts;
        $postsNicks = new \Perles\Model\PostsNicks;

        /**
         * Deal with the identifier
         *
         * Default = the newest pearl
         */
        $post = $posts->collection()->where('active', 1)->last();

        if ($identifier = $this->controller->param(0)) {
            $collection = $posts->collection()->where('active', 1)->where('created', date('c', (int)$identifier));

            if ($collection->count()) {
                $post = $collection->first();
            } else {
                /**
                 * Redirect to not-found page if pearl doesn't exist
                 */
                $this->controller->redirect(404);
            }
        }

        $olderPost = $posts->collection()->whereRaw("`id` = (SELECT max(`id`) FROM `posts` WHERE `id` < {$post->id} AND `active` = 1)")->getOne();
        $newerPost = $posts->collection()->whereRaw("`id` = (SELECT min(`id`) FROM `posts` WHERE `id` > {$post->id} AND `active` = 1)")->getOne();

        return [
            'post' => $post,
            'newerPost' => $newerPost,
            'olderPost' => $olderPost,
        ];
    }

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
