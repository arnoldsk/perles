<?php

namespace Perles\Model;

class PostsNicks extends Model
{
    public $table = 'posts_nicks';

    public $fillable = [
        'post_id', 'nicks',
    ];
}
