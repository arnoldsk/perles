<?php

require __DIR__ . '/Shell.php';

class NewsletterEmails extends Shell
{
    public function run()
    {
        $config = new \Perles\Model\Config;
        $mailer = new \Perles\Model\Mailer;
        $newsletter = new \Perles\Model\Newsletter;
        $posts = new \Perles\Model\Posts;

        // Get newsletters that have not been sent this week
        $newsletters = $newsletter->collection()
            ->whereRaw('WEEK(`last_sent_date`) != WEEK(NOW())')
            ->orWhere('last_sent_date', ['IS NULL'])
            ->get();

        $postsThisWeek = $posts->collection()
            ->whereRaw('`created` > DATE_SUB(NOW(), INTERVAL 1 WEEK)')
            ->count();

        if (!$postsThisWeek) {
            // Do nothing if no posts this week
            return;
        }
    
        foreach ($newsletters as $item) {
            $status = $mailer->send($item->email, 'newsletter', [
                'PEARL_COUNT' => $postsThisWeek,
                'BASE_URL' => $config->get('url'),
                'RECIPIENT_EMAIL' => $item->email,
            ]);

            // Update last sent date
            if ($status) {
                $newsletter->update($item->id, [
                    'last_sent_date' => 'NOW()',
                ]);
            }

            $this->writeln('Trying to send an e-mail to: ' . $item->email);
            $this->writeln($status ? 'Success' : 'Failed');
        }
    }
}

(new NewsletterEmails())->run();
