<?php

namespace Perles\Block;

class NotFound extends Block
{
    public $template = 'not-found.twig';

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
