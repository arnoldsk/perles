<?php

namespace Perles\Controller;

/**
 * Holy f@#k this file is a mess
 */
class Router extends Controller
{
    protected $route;

    protected $controller;
    
    public $controllerName;

    public $action = 'index';

    protected $parameters = [];

    protected $session;

    public function __construct($routes)
    {
        $this->session = new \Perles\Model\Session;

        if (isset($_GET['url'])) {
            $this->route = '/' . trim(filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL), '/');
        } else {
            $this->route = '/';
        }

        $routes = $this->formatRoutes($routes);

        if ($routeData = $this->getMatchingRouteData($routes)) {
            $route = $routes[$routeData['route']];
            if ($routeData['parameter']) {
                $this->parameters[] = $routeData['parameter'];
            }

            $this->controllerName = $route['controllerName'];
            $this->controller = new $route['controller']($this, $this->parameters);
            $this->action = $route['action'];
        }

        $this->beforeCall();
        call_user_func([$this->controller, $this->action]);
        $this->afterCall();
    }

    public function redirect($route = null)
    {
        $config = new \Perles\Model\Config;
        $location = trim($config->get('url'), '/');

        // Check if referrer is given
        // POST always before GET
        if (isset($_POST['ref'])) {
            $referrer = $_POST['ref'];
        } elseif (isset($_GET['ref'])) {
            $referrer = $_GET['ref'];
        } else {
            $referrer = null;
        }

        if ($route && !in_array($route, ['', '/', 'home'])) {
            $location .= '/' . trim($route, '/');
        } elseif ($route == 404) {
            $location .= '/404';
        } elseif ($referrer) {
            $location = $referrer;
        }

        header('Location: ' . $location);
        die;
    }

    protected function formatRoutes($routes)
    {
        foreach ($routes as $route => $method) {
            $routes[$route] = [];

            if (preg_match('/\{(.*)\}/', $route, $matches)) {
                unset($routes[$route]);
                $route = str_replace($matches[0], '', $route);
                $routes[$route]['parameter'] = '/^' . str_replace('/', '\/', $route) . '(' . $matches[1] . ')?$/';
            } else {
                $routes[$route]['parameter'] = '/^' . str_replace('/', '\/', $route) . '$/';
            }

            $routes[$route]['method'] = $method;

            $methodData = explode('::', $method);
            $routes[$route]['controllerName'] = $methodData[0];
            $routes[$route]['controller'] = '\Perles\Controller\\' . $routes[$route]['controllerName'];
            $routes[$route]['action'] = $methodData[1];
        }
        return $routes;
    }

    protected function getMatchingRouteData($routes)
    {
        foreach ($routes as $route => $data) {
            if (preg_match($data['parameter'], $this->route, $matches)) {
                return [
                    'route' => $route,
                    'parameter' => isset($matches[1]) ? $matches[1] : null,
                ];
            }
        }
        return null;
    }

    protected function beforeCall()
    {
        // This kind of gets called randomly?
        if (!$this->controller || !$this->action) {
            $this->redirect(404);
        }
    }

    protected function afterCall()
    {
        $this->session->uns('message');
    }
}
