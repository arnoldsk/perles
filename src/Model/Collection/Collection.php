<?php

namespace Perles\Model\Collection;

abstract class Collection
{
    protected $model;

    protected $orderField;
    protected $orderDirection = 'ASC';
    protected $limitCount;
    protected $limitOffset;

    protected $select;
    protected $where;
    protected $bindables = [];

    public function __construct(\Perles\Model\Model $model)
    {
        $this->orderField = $model->identifier;
        $this->model = $model;
    }

    public function orderBy($field, $direction = 'ASC')
    {
        $this->orderField = $field;
        $this->orderDirection = $direction;
        return $this;
    }

    public function select($columns)
    {
        $pieces = [];
        if (is_array($columns)) {
            foreach ($columns as $column) {
                $pieces[] = $column;
            }
        } else {
            $pieces[] = $columns;
        }

        $pieces = '`' . implode('`, `', $pieces) . '`';
        if ($this->select) {
            $this->select .= $pieces;
        } else {
            $this->select = $pieces;
        }

        return $this;
    }

    public function where($field, $arguments)
    {
        $pieces = [];
        if (is_array($arguments)) {

            foreach ($arguments as $operator => $value) {
                if (is_int($operator)) {
                    $pieces[] = "`{$field}` {$value}";
                } else {
                    $pieces[] = "`{$field}` {$operator} ?";
                    $this->bindables[] = $value;
                }
            }
        } else {
            $pieces[] = "`{$field}` = ?";
            $this->bindables[] = $arguments;
        }

        $pieces = implode(' OR ', $pieces);
        if ($this->where) {
            $this->where .= ' AND ' . $pieces;
        } else {
            $this->where = $pieces;
        }

        return $this;
    }

    /**
     * Forced OR operator
     * This is a hacky solution so it has to be attached at the very end of all other "where" calls
     */
    public function orWhere($field, $arguments)
    {
        $pieces = [];
        if (is_array($arguments)) {

            foreach ($arguments as $operator => $value) {
                if (is_int($operator)) {
                    $pieces[] = "`{$field}` {$value}";
                } else {
                    $pieces[] = "`{$field}` {$operator} ?";
                    $this->bindables[] = $value;
                }
            }
        } else {
            $pieces[] = "`{$field}` = ?";
            $this->bindables[] = $arguments;
        }

        $pieces = implode(' OR ', $pieces);
        if ($this->where) {
            $this->where .= ' OR ' . $pieces;
        } else {
            $this->where = $pieces;
        }

        return $this;
    }

    public function whereRaw($sql)
    {
        if ($this->where) {
            $this->where .= ' AND ' . $sql;
        } else {
            $this->where = $sql;
        }

        return $this;
    }

    public function limit($count, $offset = 0)
    {
        $this->limitCount = (int)$count;
        $this->limitOffset = (int)$offset;

        return $this;
    }

    public function getQuery()
    {
        $select = $this->select ?: '*';
        $wherePiece = $this->where ? " WHERE {$this->where}" : "";
        $orderPiece = $this->orderField ? " ORDER BY `{$this->orderField}` {$this->orderDirection}" : "";
        $limitPiece = $this->limitCount ? " LIMIT {$this->limitCount}" : "";
        $offsetPiece = $this->limitOffset ? " OFFSET {$this->limitOffset}" : "";
        $query = "SELECT {$select} FROM `{$this->model->table}`{$wherePiece}{$orderPiece}{$limitPiece}{$offsetPiece}";
        return $query;
    }

    /**
     * Never use this to get data as it does not prepare or escape anything
     */
    public function getFilledQuery()
    {
        $query = $this->getQuery();
        $bindables = $this->bindables;

        if (preg_match('/(\?)/', $query, $fillables) !== false) {
            $parts = explode('?', $query);
            $query = "";

            for ($i = 0; $i < count($parts); $i++) {
                $value = isset($bindables[$i]) ? $bindables[$i] : null;
                $query .= $parts[$i];
                if ($value) {
                    $query .= (!is_numeric($value) ? "'" . $value . "'" : $value);
                }
            }
        }

        return $query;
    }

    public function first()
    {
        $this->orderBy($this->orderField, 'ASC');
        $this->limit(1);

        $query = $this->getQuery();
        $stmt = $this->model->prepare($query);
        $stmt->execute($this->bindables);

        return $this->getOne($stmt);
    }

    public function last()
    {
        $this->orderBy($this->orderField, 'DESC');
        $this->limit(1);

        $query = $this->getQuery();
        $stmt = $this->model->prepare($query);
        $stmt->execute($this->bindables);

        return $this->getOne($stmt);
    }

    public function count()
    {
        $query = $this->getQuery();
        $query = preg_replace('/SELECT .* FROM/', "SELECT count(`{$this->model->identifier}`) as `count` FROM", $query);

        $stmt = $this->model->prepare($query);
        $stmt->execute($this->bindables);

        if ($stmt != false) {
            return (int)$stmt->fetchObject()->count;
        }
        return null;
    }

    public function get()
    {
        $query = $this->getQuery();
        $stmt = $this->model->prepare($query);
        $stmt->execute($this->bindables);

        return $this->getRows($stmt);
    }

    public function getOne()
    {
        $query = $this->getQuery();
        $stmt = $this->model->prepare($query);
        $stmt->execute($this->bindables);

        if ($rows = $this->getRows($stmt)) {
            return $rows[0];
        }
        return null;
    }

    /**
     * @param \PDOStatement $stmt
     * @return array
     */
    public function getRows(\PDOStatement $stmt)
    {
        $this->beforeGetRows($stmt);
        $rows = [];
        if ($stmt) {
            while ($row = $stmt->fetchObject()) {
                $rows[] = $row;
            }
        }
        $this->afterGetRows($rows);
        return $rows;
    }

    protected function beforeGetRows(\PDOStatement &$stmt)
    {
    }

    protected function afterGetRows(array &$rows)
    {
    }
}
