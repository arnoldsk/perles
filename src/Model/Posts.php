<?php

namespace Perles\Model;

use Perles\Model\PostsNicks;
use Perles\Model\Users;

class Posts extends Model
{
    public $table = 'posts';

    public $fillable = [
        'user_id', 'url', 'original_url', 'description', 'type', 'active', 'ip', 'created',
    ];

    public function load($value, $field = null)
    {
        $post = parent::load($value, $field);

        if (!$post) {
            return null;
        }

        if ($post->user_id >= 0) {
            $users = new Users;
            $post->user = $users->load($post->user_id);
        } else {
            $post->user = null;
        }

        return $post;
    }

    public function update($id, array $data)
    {
        $updated = parent::update($id, $data);

        if ($updated && isset($data['nicks'])) {
            $nicks = array_map('trim', explode(',', $data['nicks']));
            $nicks = implode(', ', $nicks);

            (new PostsNicks)->createOrUpdate('post_id', [
                'post_id' => $id,
                'nicks' => $nicks,
            ]);
        }
    }

    /**
     * Formats the output of pearl's description
     * Order of actions is important!
     *
     * @param  string $text
     * @return string
     */
    function formatDescription($text) {
        // Automatic anchor tags
        $text = preg_replace('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i', '<a href="$0" target="_blank">$0</a>', $text);

        // Simplify exs miniblog url
        $text = preg_replace('/<a.*>(.*exs\.lv\/say\/\d+\/\d+-(.+))<\/a>/i', '<a href="$1" target="_blank">$2...</a>', $text);

        // New lines
        $text = nl2br($text);

        return $text;
    }

    public function getPostCountThisWeek()
    {
        $yearweek = date('YW');
        $stmt = $this->prepare("SELECT count(*) as count FROM `{$this->table}` WHERE YEARWEEK(created) = ?");
        $stmt->execute([$yearweek]);

        if ($stmt) {
            return $stmt->fetchObject()->count;
        }
        return 0;
    }

    protected function beforeDelete($id)
    {
        if ($post = $this->load($id)) {
            $config = new \Perles\Model\Config;
            /**
             * Delete images from local/share folders
             */
            if (strpos($post->url, 'share.arnoldsk.lv/files/perles/') !== false) {
                $dir = BASE_DIR . '../share/files/perles';
            } elseif (strpos($post->url, $config->get('url')) !== false) {
                $dir = PUBLIC_DIR . 'assets/images/perles';
            }

            if (isset($dir)) {
                $filename = $dir . '/' . basename($post->url);
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }
        }

        return parent::beforeDelete($id);
    }

    protected function beforeCreate(&$data)
    {
        /**
         * Fetch and update user data
         */
        if (isset($data['user_id'])) {
            $userId = (int)$data['user_id'];

            (new Users)->createOrUpdateFromApi($userId);
        }

        return parent::beforeCreate($data);
    }
}
