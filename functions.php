<?php

/**
 * http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
 */
{
    // Returns a file size limit in bytes based on the PHP upload_max_filesize
    // and post_max_size
    function file_upload_max_size() {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $max_size = parse_size(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }
}

/**
 * Get an email template
 */
function getEmailHtml($name, $params = []) {
    $file = BASE_DIR . '/views/email/' . $name . '.html';

    if (!file_exists($file)) {
        return '';
    }

    $html = file_get_contents($file);

    if ($params) {
        foreach ($params as $param => $value) {
            $param = '{{' . strtoupper($param) . '}}';
            if (strpos($html, $param) !== false) {
                $html = str_replace($param, $value, $html);
            }
        }
    }

    return $html;
}

/**
 * Log to a file
 */
function var_log($name, $string) {
    $file = BASE_DIR . '/var/' . $name . '.log';
    umask(0);
    file_put_contents($file, date('c') . ' - ' . $string . PHP_EOL, FILE_APPEND);
}

/**
 * Email validation
 */
function validateEmail($email) {
    return !!preg_match('/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD', $email);
}

/**
 * Send an email
 */
function sendEmail($emailAddress, $html) {
    global $config;

    $mail = new PHPMailer;

    $mail->SMTPDebug = $config['mail.debug'];
    $mail->Debugoutput = 'html';

    $mail->CharSet = 'UTF-8';
    $mail->Host = 'smtp.gmail.com';
    $mail->Username = $config['mail.username'];
    $mail->Password = $config['mail.password'];
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->setFrom($config['mail.username'], 'Exs Pērles');

    //////

    $mail->addAddress($emailAddress);

    $mail->Subject = 'Exs Pērles';
    $mail->msgHTML($html);
    $mail->AltBody = strip_tags($html);

    return $mail->send();
}

function getHexBrightness($hex, $returnType = false, $compareAmount = 382) {
    $hex = ltrim($hex, '#');

    //break up the color in its RGB components
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));

    $brightness = $r + $g + $b;

    if (!$returnType) {
        return $brightness;
    }

    //do simple weighted average
    //
    //(This might be overly simplistic as different colors are perceived
    // differently. That is a green of 128 might be brighter than a red of 128.
    // But as long as it's just about picking a white or black text color...)
    if($brightness > $compareAmount){
        return 'bright';
    } else {
        return 'dark';
    }
}

    
