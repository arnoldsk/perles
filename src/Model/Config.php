<?php

namespace Perles\Model;

class Config
{
    private $data;

    public function __construct()
    {
        $this->data = require BASE_DIR . 'config.php';
    }

    public function all()
    {
        return $this->data;
    }

    public function get($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    public function set($key, $data)
    {
        $this->data[$key] = $data;
        return $this;
    }

    public function uns($key)
    {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }
        return $this;
    }
}
