<?php

namespace Perles\Controller;

abstract class Controller
{
    protected $router;

    protected $parameters;

    protected $session;

    public function __construct(\Perles\Controller\Router $router, $parameters = [])
    {
        $this->router = $router;
        $this->parameters = $parameters;
        $this->session = new \Perles\Model\Session;

        $this->checkAuthPersistance();

        $this->init();
    }

    /**
     * Rewrite this method instead of __construct
     * to include custom properties or auth checks
     */
    protected function init()
    {
        // return parent::init();
    }

    protected function authOnly()
    {
        if (!$this->session->get('login')) {
            $this->session->set('message', 'Nepieciešams ielogoties, lai apmeklētu vēlamo lapu.');
            $this->redirect('login');
        }
    }

    protected function guestOnly()
    {
        if ($this->session->get('login')) {
            $this->session->set('message', 'Šī lapa ir pieejama tikai neielogotiem lietotājiem.');
            $this->redirect();
        }
    }

    protected function adminOnly()
    {
        if (!$this->session->get('admin')) {
            $this->session->set('message', 'Šī lapa ir pieejama tikai administrātoriem.');
            $this->redirect();
        }
    }

    protected function checkAuthPersistance()
    {
        $cookies = new \Perles\Model\Cookies;
        $tokens = new \Perles\Model\Tokens;

        if ($token = $cookies->get('persistent') && !$this->session->get('login')) {
            foreach ($tokens->collection()->get() as $item) {
                if (!password_verify($token, $item->hash)) {
                    continue;
                }

                // Token found
                $this->session->set('login', true);
                $this->redirect();
            }

            // Failed to find a valid token
            $cookies->uns('persistent');
            $this->session->set('message', 'Neizdevās ienākt automātiski.');
            $this->redirect();
        }
    }

    public function param($key)
    {
        if (isset($this->parameters[$key])) {
            return $this->parameters[$key];
        }
        return null;
    }

    public function params()
    {
        return $this->parameters;
    }

    public function redirect($route = null)
    {
        $this->router->redirect($route);
    }

    public function getControllerName()
    {
        return $this->router->controllerName;
    }

    public function getAction()
    {
        return $this->router->action;
    }

    public function getFullAction()
    {
        return strtolower($this->getControllerName() . '-' . $this->getAction());
    }
}
