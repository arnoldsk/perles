<?php

namespace Perles\Model;

class Cookies
{
    public function get($key)
    {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
        return null;
    }

    public function set($key, $data, $expire = false)
    {
        setcookie($key, $data, ($expire === false ? 2000000000 : $expire));
        return $this;
    }

    public function uns($key)
    {
        if (isset($_COOKIE[$key])) {
            setcookie($key, '', 1);
        }
        return $this;
    }
}
