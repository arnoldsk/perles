<?php

namespace Perles\Block\Layout;

class Footer extends \Perles\Block\Block
{
    public $template = 'layout/footer.twig';
}
