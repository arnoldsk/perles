<?php

require __DIR__ . '/Shell.php';

class MigrateImages extends Shell
{
    protected $posts;
    protected $localImageDir;
    protected $shareImageDir;
    protected $config;

    public function run()
    {
        $this->posts = new \Perles\Model\Posts;
        $this->localImageDir = PUBLIC_DIR . 'assets/images/perles';
        $this->shareImageDir = PUBLIC_DIR . 'screens';
        $thus->config = new \Perles\Model\Config;

        $this->checkShareDir();

        $this->moveLocalImages();
        $this->moveExternalImages();
    }

    protected function checkShareDir()
    {
        if (!file_exists($this->shareImageDir)) {
            die(
                $this->writeln('Share directory not found.')
            );
        }
    }

    protected function getLocalImages()
    {
        return glob($this->localImageDir . '/*');
    }

    protected function getPostByImage($basename)
    {
        $collection = $this->posts->collection()
            ->where('active', 1)
            ->where('url', ['like' => '%/' . $basename])
            ->select(['id', 'url']);

        if ($collection->count()) {
            return $collection->getOne();
        }
        return null;
    }

    protected function moveLocalImages()
    {
        $postIdsMoved = [];

        foreach ($this->getLocalImages() as $localImage) {
            $basename = basename($localImage);

            if ($post = $this->getPostByImage($basename)) {
                // Move the image
                rename($localImage, $this->shareImageDir . '/' . $basename);

                // Update post
                $this->posts->update($post->id, [
                    'original_url' => $post->url,
                    'url' => $this->config->get('url') . '/screens/' . $basename,
                ]);

                $postIdsMoved[] = $post->id;
            }
        }

        if ($postIdsMoved) {
            $this->writeln('Moved local images for IDs: ' . implode(', ', $postIdsMoved));
        } else {
            $this->writeln('No local images found.');
        }
    }

    protected function moveExternalImages()
    {
        $postIdsMoved = [];

        $externalImagePosts = $this->posts->collection()
            // Not checking for local images because they are moved beforehand
            ->where('url', ['not like' => '%' . $this->config->get('url') . '/screens/%'])
            ->where('url', ['not like' => '%youtube.com%'])
            ->where('active', 1)
            ->select(['id', 'url', 'created'])
            ->get();

        foreach ($externalImagePosts as $post) {
            $basename = strtotime($post->created) . '.jpg';

            // Move the image
            if ($image = @file_get_contents($post->url)) {
                // "Download" it
                file_put_contents($this->shareImageDir . '/' . $basename, $image);

                // Update post
                $this->posts->update($post->id, [
                    'original_url' => $post->url,
                    'url' => $this->config->get('url') . '/screens/' . $basename,
                ]);

                $postIdsMoved[] = $post->id;
            }
        }

        if ($postIdsMoved) {
            $this->writeln('Moved external images for IDs: ' . implode(', ', $postIdsMoved));
        } else {
            $this->writeln('No external images found.');
        }
    }
}

(new MigrateImages())->run();
