<?php

namespace Perles\Model;

class Twig
{
    protected $twig;

    public function __construct(\Perles\Block\Block $block, $data = [])
    {
        $this->block = $block;

        $loader = new \Twig_Loader_Filesystem($block->viewPath);
        $twig = new \Twig_Environment($loader, [
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'auto_reload' => true,
        ]);

        $twig->addGlobal('block', $block);
        $twig->addGlobal('session', $block->session);
        $twig->addGlobal('config', $block->config);
        $twig->addGlobal('base_url', $block->config->get('url'));
        $twig->addGlobal('helper', new \Perles\Helper);
        $twig->addGlobal('request', [
            'get' => $_GET,
            'post' => $_POST,
        ]);

        if ($block->template) {
            $twig->display($block->template, $data);
        }
    }
}
