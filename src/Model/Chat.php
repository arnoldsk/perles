<?php

namespace Perles\Model;

class Chat extends Model
{
    public $table = 'chat';

    public $fillable = [
        'name', 'content', 'admin', 'ip', 'created',
    ];

    /**
     * Formats the output of chat messages
     * 
     * @param  string $text
     * @return string
     */
    function formatChatMessage($content) {
        // Automatic anchor tags
        $content = preg_replace('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i', '<a href="$0" target="_blank">$0</a>', $content);

        // Simplify exs miniblog url
        $content = preg_replace('/<a.*>(.*exs\.lv\/say\/\d+\/\d+-(.+))<\/a>/i', '<a href="$1" target="_blank">$2...</a>', $content);

        return $content;
    }
}
