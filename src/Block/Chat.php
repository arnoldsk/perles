<?php

namespace Perles\Block;

class Chat extends Block
{
    public $template = 'chat.twig';

    protected function data()
    {
        return [
            'messages' => $this->getMessages(),
        ];
    }

    protected function getMessages()
    {
        $chat = new \Perles\Model\Chat;
        $messages = [];
        $messages = $chat->collection()->orderBy('created', 'DESC')->limit(20)->get();

        // Reverse the order
        /*if ($_messages) {
            foreach ($_messages as $message) {
                array_unshift($messages, $message);
            }
        }*/

        return $messages;
    }
}
