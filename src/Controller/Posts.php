<?php

namespace Perles\Controller;

class Posts extends Controller
{
    /**
     * @var \Perles\Model\Helper $helper
     */
    protected $helper;

    /**
     * @var \Perles\Model\Posts $model
     */
    protected $model;

    /**
     * @var \Perles\Model\Config $config
     */
    protected $config;

    /**
     * @var \Perles\Model\Newsletter $newsletter
     */
    protected $newsletter;

    /**
     * @var \Perles\Model\Mailer $mailer
     */
    protected $mailer;

    protected function init()
    {
        $this->helper = new \Perles\Helper;
        $this->model = new \Perles\Model\Posts;
        $this->config = new \Perles\Model\Config;
        $this->newsletter = new \Perles\Model\Newsletter;
        $this->mailer = new \Perles\Model\Mailer;

        return parent::init();
    }

    public function send()
    {
        // $this->authOnly();

        new \Perles\Block\Send($this);
    }

    public function sendPost()
    {
        $errors = [];

        $userId = isset($_POST['user_id']) && $_POST['user_id'] > 0 ? (int)$_POST['user_id'] : 0;
        $image = isset($_POST['image']) ? $_POST['image'] : null;
        $upload = isset($_FILES['upload']) ? $_FILES['upload'] : null;
        if ($upload['name']) {
            $image = $upload['tmp_name'];
        } else {
            $upload = null;
        }

        $description = isset($_POST['description']) ? trim($_POST['description']) : null;
        $setAsActive = isset($_POST['set_as_active']) && $this->session->get('admin');

        // Check the description
        if (!$description) {
            $errors[] = 'Apraksts ir obligāts!';
        } elseif (!$this->helper->textContainsUrl($description)) {
            $errors[] = 'Aprakstā ir jābūt ieraksta linkam.';
        }

        /**
         * Check image extension, getimagetype and mime-type
         */
        if ($upload) {
            $imageCheck = $this->helper->validateImageExtension($upload['name'])
                && $this->helper->validateImageMimeType($image)
                && getimagesize($image)
                && $upload['size'] <= $this->helper->getMaxUploadSize();
        } elseif ($image) {
            $imageCheck = getimagesize($image) !== false;
        } else {
            $imageCheck = false;
        }

        if (!$imageCheck) {
            $errors[] = 'Nepareizs attēls/neizdevās augšupielādēt.';
        }

        if (empty($errors)) {
            // For consistent timestamps, store the initial date
            $date = date('Y-m-d H:i:s');

            // Save the uploaded image
            if ($upload) {
                $ext = explode('.', $upload['name']);
                $ext = end($ext);

                $dir = PUBLIC_DIR . '/assets/images/perles/';
                $filePath = $dir . strtotime($date) . '.' . $ext;

                umask(0);
                move_uploaded_file($image, $filePath);

                // Just to keep stuff consistent, I'm replacing image with the relative url to the image.
                $image = $this->config->get('url') . '/assets/images/perles/' . basename($filePath);
            }

            // Create database entry
            $saved = $this->model->create([
                'user_id' => $userId,
                'url' => $image,
                'original_url' => $image,
                'description' => $description,
                'type' => 'image',
                'active' => $setAsActive ? 1 : 0,
                'ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '',
                'created' => $date,
            ]);

            if ($saved && !$setAsActive) {
                // Send an email of the new post
                $email = $this->config->get('mail.contact');
                $this->mailer->send($email, 'review', [
                    'image' => $image,
                    'description' => $description,
                    'base_url' => $this->config->get('url'),
                ]);
            } else {
                // Delete the uploaded file
                $this->helper->deleteUploadedImage(basename($image));
            }

            $this->session->set('message', $setAsActive ? 'Attēls pievienots.' : 'Attēls pievienots apskatīšanai.');
        } else {
            $this->session->set('message', implode('<br>', $errors));
        }

        $this->redirect('/send');
    }

    public function review()
    {
        $this->authOnly();
        $this->adminOnly();

        new \Perles\Block\Review($this);
    }

    public function reviewAccept()
    {
        $this->authOnly();
        $this->adminOnly();
        $id = (int)$this->param(0);

        if ($post = $this->model->load($id)) {
            $this->model->update($post->id, [
                'active' => 1,
            ]);
            $this->session->set('message', 'Attēls pieņemts.');
        }

        $this->redirect('/review');
    }

    public function reviewReject()
    {
        $this->authOnly();
        $this->adminOnly();
        $id = (int)$this->param(0);

        if ($post = $this->model->load($id)) {
            $this->helper->deleteUploadedImage(basename($post->url));
            $this->model->delete($post->id);
            $this->session->set('message', 'Attēls noraidīts.');
        }

        $this->redirect('/review');
    }

    public function updatePost()
    {
        $this->authOnly();
        $this->adminOnly();

        if (isset($_POST['id'])) {
            $id = (int)$_POST['id'];

            $this->model->update($id, $_POST);
            $this->session->set('message', 'Izmaiņas saglabātas.');
        }

        $this->redirect();
    }

    public function delete()
    {
        $this->authOnly();
        $this->adminOnly();
        $id = (int)$this->param(0);

        if ($post = $this->model->load($id)) {
            $this->model->delete($post->id);
            $this->session->set('message', 'Attēls izdzēsts.');
        }

        $this->redirect();
    }
}
