<?php

namespace Perles\Model;

abstract class Database extends \PDO
{
    public function __construct()
    {
        $config = new \Perles\Model\Config;
        $details = [
            'host=' . ($config->get('db.host') ?: 'localhost'),
            'dbname=' . $config->get('db.name'),
            'charset=utf8',
        ];

        parent::__construct('mysql:' . implode(';', $details), $config->get('db.user'), $config->get('db.pass'));
    }
}
