<?php

namespace Perles\Model;

class Tokens extends Model
{
    public $table = 'tokens';

    public $fillable = [
        'hash',
    ];
}
