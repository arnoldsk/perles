var Chat = function(selector) {
    this.chat = $(selector);
    this.messages = this.chat.find('.messages');
    this.name = this.chat.find('.input[name=name]');
    this.content = this.chat.find('.input[name=content]');
    this.interval = null;

    this.init = function() {
        // Position
        this.chat.find('.chat-header').on('click', function() {
            this.chat.toggleClass('active');
            this.updateChatPosition();
        }.bind(this));

        // Set name field value
        if (this.getCookie('chat_name')) {
            this.name.val(this.getCookie('chat_name'));
        }

        // Load messages
        this.messages.load(this.chat.attr('action'));

        // Submit on enter key press
        this.content.on('keydown', function(e) {
            if (e.which == 13) {
                if (this.content.val().length) {
                    this.chat.submit();
                } else {
                    e.preventDefault();
                }
            }
        }.bind(this));

        // Send a chat message
        this.chat.on('submit', function(e) {
            e.preventDefault();

            this.updateMessages();
        }.bind(this));

        // Block
        $(document).on('click', '.chat-block-ip', function() {
            const ip = $(this).data('ip');

            if (confirm(`${ip}\nVai bloķēt šo IP adresi?`)) {
                $.post(`${BASE_URL}/chat/block`, { ip });
            }
        });
    };

    this.stopAutoload = function() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    };

    this.startAutoload = function() {
        this.stopAutoload();
        this.interval = setInterval(function() {
            this.messages.load(this.chat.attr('action'));
        }.bind(this), 2000);
    };

    this.scrollMessages = function() {
        // Not needed anymore since we don't reverse message display order
        // this.messages.scrollTop(this.messages[0].scrollHeight);
    };

    this.updateChatPosition = function() {
        // Disable writing
        this.content.prop('disabled', true);

        if (this.chat.hasClass('active')) {
            // Enable writing
            this.content.prop('disabled', false).focus();

            // Start autoload
            this.startAutoload();

            this.chat.animate({
                top: $(window).height() - this.chat.outerHeight()
            }, 300);
        } else {
            // Stop autoload
            this.stopAutoload();

            this.chat.animate({
                top: '100%'
            }, 300);
        }

        this.scrollMessages();
    };

    this.updateMessages = function(data) {
        $.ajax({
            url: this.chat.attr('action'),
            type: 'post',
            data: this.chat.serialize(),
            success: function(html) {
                $('#chat .messages').html(html);
                $('#chat .input[name=content]').val('');

                this.updateChatPosition();
                this.startAutoload();
            }.bind(this)
        });
    };

    this.getCookie = function(name) {
        var value = '; ' + document.cookie;
        var parts = value.split('; ' + name + '=');
        if (parts.length == 2) {
            var part = parts.pop().split(';').shift();
            return part.replace('+', ' ');
        }
        return null;
    }

    this.init();
};
