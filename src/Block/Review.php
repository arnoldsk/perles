<?php

namespace Perles\Block;

class Review extends Block
{
    public $template = 'posts/review.twig';

    protected function data()
    {
        $posts = new \Perles\Model\Posts;
        $postCollection = $posts->collection()->where('active', 0)->orderBy('created', 'desc');

        return [
            'postCollection' => $postCollection,
        ];
    }

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
