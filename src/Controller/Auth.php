<?php

namespace Perles\Controller;

class Auth extends Controller
{
    /**
     * @var \Perles\Helper $helper
     */
    protected $helper;

    /**
     * @var \Perles\Model\Tokens $tokens
     */
    protected $tokens;

    /**
     * @var \Perles\Model\Cookies $cookies
     */
    protected $cookies;

    /**
     * @var \Perles\Model\Passwords $passwords
     */
    protected $passwords;

    /**
     * @var \Perles\Model\Users $users
     */
    protected $users;

    protected function init()
    {
        $this->helper = new \Perles\Helper;
        $this->tokens = new \Perles\Model\Tokens;
        $this->cookies = new \Perles\Model\Cookies;
        $this->passwords = new \Perles\Model\Passwords;
        $this->users = new \Perles\Model\Users;
    }

    public function login()
    {
        $this->guestOnly();

        new \Perles\Block\Login($this);
    }

    public function loginPost()
    {
        $this->guestOnly();

        $result = isset($_POST['result']) ? json_decode($_POST['result']) : null;

        if ($result) {
            if (isset($result->error)) {
                $this->session->set('message', $result->error);

                $this->redirect('login');
            } else {
                $user = $result->data;

                $this->session->set('login', true);
                $this->session->set('user', $user);

                // Create user
                $this->users->createOrUpdateFromApi($user->id);

                // Verify if it's admin
                $user = $this->users->load($user->id);
                if ((bool)$user->admin) {
                    $this->session->set('admin', true);
                }

                $this->redirect();
            }
        }

        $this->session->set('message', 'Neizdevās ielogoties.');
        $this->redirect('login');
    }

    protected function createToken()
    {
        /**
         * Create new, random token string
         */
        $token = $this->helper->getRandomString();

        /**
         * Set a cookie with the token string
         */
        $this->cookies->set('persistent', $token);

        /**
         * Save token hash in database
         */
        $this->tokens->create([
            'hash' => password_hash($token, PASSWORD_BCRYPT),
        ]);
    }

    public function logout()
    {
        $this->session->uns('login');
        $this->session->uns('user');
        $this->session->uns('admin');
        $this->redirect('/login');
    }
}
