<?php

namespace Perles\Block;

class Newsletter extends Block
{
    public $template = 'newsletter.twig';

    protected function data()
    {
        $newsletter = new \Perles\Model\Newsletter;
        $posts = new \Perles\Model\Posts;
        return [
            'emailsCollection' => $newsletter->collection(),
            'subscriber' => $newsletter->getSubscriber(),
            'postCountThisWeek' => $posts->getPostCountThisWeek(),
        ];
    }

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
