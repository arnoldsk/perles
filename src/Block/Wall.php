<?php

namespace Perles\Block;

class Wall extends Block
{
    public $template = 'wall.twig';

    protected function data()
    {
        $model = new \Perles\Model\Posts;

        if (isset($_GET['nick'])) {
            $nick = trim($_GET['nick']);

            if (!strlen($nick)) {
                $this->controller->redirect('/wall');
            }

            // join nicks and search for it in an ugly way
            $stmt = $model->prepare("
                SELECT `posts`.*, `posts_nicks`.`nicks` as `nicks`
                FROM `posts`
                LEFT JOIN `posts_nicks`
                ON `posts_nicks`.`post_id` = `posts`.`id`
                WHERE `nicks` LIKE ? AND `active` = 1
                ORDER BY `created` DESC
            ");
            $stmt->execute(['%' . $nick . '%']);

            $posts = $model->collection()->getRows($stmt);
        } else {
            $posts = $model->collection()->where('active', 1)->orderBy('created', 'desc')->get();
        }

        return [
            'posts' => $posts,
        ];
    }

    protected function beforeRender()
    {
        new \Perles\Block\Layout\Header($this->controller);
        parent::beforeRender();
    }

    protected function afterRender()
    {
        new \Perles\Block\Layout\Footer($this->controller);
        parent::afterRender();
    }
}
