<?php

namespace Perles\Controller;

class Home extends Controller
{
    public function index()
    {
        new \Perles\Block\Home($this);
    }

    public function admin()
    {
        die('<body style="margin:0;background:#000;"><img src="http://i.imgur.com/ssnYRYJ.jpg" style="height:100vh;margin:auto;display:block;" alt="Jobans esi?"></body>');
    }

    public function wall()
    {
        new \Perles\Block\Wall($this);
    }

    public function notFound()
    {
        new \Perles\Block\NotFound($this);
    }
}
