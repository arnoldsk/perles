<?php

return [
    '/{[0-9]{10}}' => 'Home::index',
    '/wall' => 'Home::wall',

    '/admin' => 'Home::admin',
    '/admin.php' => 'Home::admin',

    '/chat' => 'Chat::index',
    '/chat/block' => 'Chat::block',

    '/login' => 'Auth::login',
    '/loginPost' => 'Auth::loginPost',
    '/logout' => 'Auth::logout',

    '/send' => 'Posts::send',
    '/sendPost' => 'Posts::sendPost',
    '/review' => 'Posts::review',
    '/review/accept/{[0-9]+}' => 'Posts::reviewAccept',
    '/review/reject/{[0-9]+}' => 'Posts::reviewReject',
    '/update' => 'Posts::updatePost',
    '/delete/{[0-9]+}' => 'Posts::delete',

    '/emails' => 'Newsletter::index',
    '/emails/subscribe' => 'Newsletter::subscribePost',
    '/emails/delete/{[0-9]+}' => 'Newsletter::delete',
    '/unsubscribe' => 'Newsletter::unsubscribe',

    '/404' => 'Home::notFound',
];
