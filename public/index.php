<?php

error_reporting(-1);
ini_set('display_errors', 'On');

session_start();
ini_set('memory_limit', '64M');

require __DIR__ . '/../vendor/autoload.php';

$app = new \Perles\Core;
$app->run();
