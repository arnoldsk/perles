<?php

namespace Perles\Model\Collection;

class Chat extends Collection
{
    protected function afterGetRows(array &$rows)
    {
        $helper = new \Perles\Helper;

        foreach ($rows as $post) {
            if (isset($post->content)) {
                $post->contentRaw = $post->content;
                $post->content = $this->model->formatChatMessage($post->contentRaw);
            }
            if (isset($post->ip)) {
                $post->color = '#' . substr(md5($post->ip), -6);
                $post->colorBrightness = $helper->getHexBrightness($post->color, true, 230);
            }
        }
    }
}
