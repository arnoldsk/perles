<?php

require __DIR__ . '/Shell.php';

class TestEmail extends Shell
{
    public function run()
    {
        $config = new \Perles\Model\Config;
        $mailer = new \Perles\Model\Mailer;

        $status = $mailer->send($config->get('mail.contact'), 'test', [
            'time' => date('Y-m-d H:i'),
        ]);

        $this->writeln('Trying to send an e-mail to: ' . $config->get('mail.contact'));
        $this->writeln($status ? 'Success' : 'Failed');
    }
}

(new TestEmail())->run();
