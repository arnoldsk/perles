<?php

namespace Perles\Controller;

class Chat extends Controller
{
    protected function checkAuthPersistance()
    {
        // Disable auth check since it's a chat...
        return;
    }

    public function index()
    {
        $this->checkPostData();

        new \Perles\Block\Chat($this);
    }

    protected function checkPostData()
    {
        /**
         * New message request
         */
        if (isset($_POST['name'], $_POST['content'])) {
            $name = trim($_POST['name']);
            $content = trim($_POST['content']);
            $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '::1';

            $blockedIps = new \Perles\Model\BlockedIps;
            $isBlocked = $blockedIps->load($ip, 'ip') != null;

            if ($name && $content && strlen($name) <= 14 && strlen($content) <= 140 && !$isBlocked) {
                $chat = new \Perles\Model\Chat;
                $status = $chat->create([
                    'name' => htmlspecialchars($name),
                    'content' => htmlspecialchars($content),
                    'admin' => $this->session->get('login') ? 1 : 0,
                    'ip' => $ip,
                    'created' => 'NOW()',
                ]);

                // Save the name in cookies
                (new \Perles\Model\Cookies)->set('chat_name', $name);
            } else {
                $error = 'Nevarēja nosūtīt ziņu!';
            }
        }
    }

    public function block()
    {
        header('Content-Type: application/json');

        $ip = isset($_POST['ip']) ? $_POST['ip'] : null;
        $result = false;

        if ($ip && $this->session->get('admin')) {
            $blockedIps = new \Perles\Model\BlockedIps;

            // Block the IP
            $blockedIps->createOrUpdate('ip', [
                'ip' => $ip,
                'created' => 'now()',
            ]);

            // Remove all related messages
            $chat = new \Perles\Model\Chat;
            $chat->delete($ip, 'ip');

            $result = true;
        }

        echo json_encode($result);
    }
}
