<?php

namespace Perles\Model;

class Users extends Model
{
    public $table = 'users';

    public $fillable = [
        'id', 'nick', 'avatar',
    ];

    /**
     * Create and update user based on EXS user data
     *
     * @param int $userId
     * @return int|null|bool honestly I don't know
     */
    public function createOrUpdateFromApi($userId)
    {
        $userInfo = json_decode(file_get_contents('https://exs.lv/user_stats/json/' . $userId));

        if ($userInfo) {
            $avatarUrl = $userInfo->avatar->medium;
            $avatarExt = explode('.', $avatarUrl);
            $avatarExt = $avatarExt[count($avatarExt) - 1];
            $avatar = $userId . '.' . $avatarExt;

            // Save the avatar locally
            file_put_contents(PUBLIC_DIR . 'assets/images/avatars/' . $avatar, file_get_contents($avatarUrl));

            return $this->createOrUpdate('id', [
                'id' => $userId,
                'nick' => $userInfo->nick,
                'avatar' => $avatar
            ]);
        }
    }
}
