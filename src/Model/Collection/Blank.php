<?php

namespace Perles\Model\Collection;

/**
 * Class used for models that don't really require
 * a collection file just for the sake of it
 */
class Blank extends Collection
{
}
