<?php

namespace Perles\Model;

class Passwords extends Model
{
    public $table = 'passwords';

    public $fillable = [
        'hash',
    ];
}
