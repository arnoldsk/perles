<?php

namespace Perles\Controller;

class Newsletter extends Controller
{
    /**
     * @var \Perles\Model\Newsletter $model
     */
    protected $model;

    /**
     * @var \Perles\Model\Mailer $mailer
     */
    protected $mailer;

    /**
     * @var \Perles\Model\Cookies $cookies
     */
    protected $cookies;

    protected function init()
    {
        $this->model = new \Perles\Model\Newsletter;
        $this->mailer = new \Perles\Model\Mailer;
        $this->cookies = new \Perles\Model\Cookies;
        return parent::init();
    }

    public function index()
    {
        new \Perles\Block\Newsletter($this);
    }

    public function subscribePost()
    {
        if (isset($_POST['email'])) {
            $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_EMAIL);

            // todo do some validation and sanitization
            if (!$this->model->validateEmail($email)) {
                $this->session->set('message', 'Lūdzu, ievadi derīgu e-pasta adresi!');
                $this->redirect('/emails');
            }

            // Check if any of the emails is disallowed
            foreach ($this->model->disallowedEmails as $badEmail) {
                if (strpos($email, $badEmail) !== false) {
                    $this->session->set('message', 'Šo e-pastu nevar izmantot!');
                    $this->redirect('/emails');
                }
            }

            if ($this->model->load($email, 'email')) {
                $this->session->set('message', 'Šis e-pasts jau ir pierakstīts jaunumiem!');
                $this->redirect('/emails');
            }

            // Create an email
            $this->model->create([
                'email' => $email,
                'created' => 'NOW()',
            ]);

            // Store it in cookies
            $this->cookies->set('newsletter', $email);

            // Send the welcome email
            $isMailSent = $this->mailer->send($email, 'welcome', [
                'base_url' => (new \Perles\Model\Config)->get('url'),
                'recipient_email' => $email,
            ]);

            if ($isMailSent) {
                $this->session->set('message', 'Izdevās! Nosūtīts apstiprinājuma e-pasts.');
            } else {
                $this->session->set('message', 'E-pasts tika pievienots, taču radās kļūda nosūtot apstiprinājuma e-pastu.');
            }
        }

        $this->redirect('/emails');
    }

    public function delete()
    {
        $this->authOnly();
        $id = (int)$this->param(0);

        if ($post = $this->model->load($id)) {
            $this->model->delete($post->id);
            $this->session->set('message', 'E-pasts izdzēsts.');
        }

        $this->redirect();
    }

    public function unsubscribe()
    {
        if (isset($_GET['email'])) {
            $email = filter_var(trim($_GET['email']), FILTER_SANITIZE_EMAIL);

            if (!$this->model->validateEmail($email)) {
                $this->redirect();
            }

            if ($subscriber = $this->model->load($email, 'email')) {
                $this->model->delete($subscriber->id);
            }
            
            $this->session->set('message', 'E-pasti vairs netiks sūtīti uz šo adresi.');
        }

        $this->redirect('/emails');
    }

    public function newsletterCron()
    {
        // There should be a shell for this
    }
}
