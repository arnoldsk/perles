<?php

require __DIR__ . '/../vendor/autoload.php';

abstract class Shell extends \Perles\Core
{
    public function __construct()
    {
        $this->verifyCli();
        parent::__construct();
    }

    public function run()
    {
        $this->writeln('Default output');
    }

    protected function writeln($text = '')
    {
        echo "{$text}\n";
    }

    /**
     * In essence, user cannot access these files anyway but just in case
     * 
     * @return void
     */
    protected function verifyCli()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            die('This script cannot be run from Browser. This is the shell script.');
        }
    }
}
