<?php

namespace Perles\Model;

class Mailer
{
    /**
     * @var PHPMailer $mail
     */
    protected $mail;

    /**
     * @var \Perles\Model\Config $config
     */
    protected $config;

    public function __construct()
    {
        $this->config = new \Perles\Model\Config;
        $this->mail = new \PHPMailer;

        $this->mail->SMTPDebug = $this->config->get('mail.debug');
        $this->mail->Debugoutput = 'html';

        $this->mail->CharSet = 'UTF-8';
        $this->mail->Host = 'smtp.gmail.com';
        $this->mail->Username = $this->config->get('mail.username');
        $this->mail->Password = $this->config->get('mail.password');
        $this->mail->Port = 587;
        $this->mail->SMTPSecure = 'tls';
        $this->mail->SMTPAuth = true;

        $this->mail->isSMTP();
        $this->mail->isHTML(true);

        $this->mail->setFrom($this->config->get('mail.username'), 'ArnoldsK');
    }

    /**
     * @return bool
     */
    public function send($email, $template, $data = [])
    {
        if ($html = $this->getTemplateHtml($template, $data)) {
            $this->mail->addAddress($email);
            $this->mail->Subject = 'Exs Pērles';
            $this->mail->msgHTML($html);
            $this->mail->AltBody = strip_tags($html);

            return $this->mail->send();
        }

        return false;
    }

    /**
     * This could be done via Twig but I cannot be bothered
     */
    public function getTemplateHtml($template, $data = [])
    {
        $file = BASE_DIR . 'views/email/' . $template . '.html';

        if (!file_exists($file)) {
            return '';
        }

        $html = file_get_contents($file);

        // Translate variables
        foreach ($data as $name => $value) {
            $name = '{{' . strtoupper($name) . '}}';
            if (strpos($html, $name) !== false) {
                $html = str_replace($name, $value, $html);
            }
        }

        return $html;
    }
}
