<?php

namespace Perles;

class Helper
{
    public function getRandomString($length = 10) {
        $characters = '_-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function deleteUploadedImage($basename) {
        umask(0);
        $filePath = PUBLIC_DIR . '/assets/images/perles/' . $basename;
        $altPath = BASE_DIR . '/../share/files/perles/'  . $basename;

        if (file_exists($filePath)) {
            unlink($filePath);
        }

        if (file_exists($altPath)) {
            unlink($altPath);
        }
    }
    
    public function uploadedImageExists($basename) {
        $filePath = BASE_DIR . '/public/assets/images/perles/' . $basename;
        $altPath = BASE_DIR . '/../share/files/perles/'  . $basename;
        return file_exists($filePath) || file_exists($altPath);
    }

    /**
     * Removes /https?:/ from an URL
     * @param  string  $url
     * @param  boolean $reverse adds /https:/ to the URL if it's got no protocol
     * @param  boolean $customPrefix use custom protocol instead of https
     * @return string
     */
    public function getProtocolRelativeUrl($url, $reverse = false, $customPrefix = false) {
        $prefix = '';
        if ($customPrefix) {
            $prefix = $customPrefix;
        } elseif (strpos($url, 'http:') !== false) {
            $prefix = 'http';
        } elseif (strpos($url, 'https:') !== false) {
            $prefix = 'https';
        }/* elseif (strpos($url, '/assets/images/perles/') !== false) {
            preg_match('/(https?):/', url(), $matches);
            if (isset($matches[1])) {
                $prefix = $matches[1];
            }
        }*/

        if ($prefix) $prefix .= ':';
        if ($reverse) return preg_replace('/^\/\//', $prefix . '//', $url);
        return preg_replace('/https?:\/\//', '//', $url);
    }

    public function validateImageExtension($filename) {
        return in_array(strtolower(substr(strrchr($filename, '.'), 1)), ['png', 'jpg', 'jpeg', 'gif']);
    }

    public function validateImageMimeType($filepath) {
        $fi = finfo_open(FILEINFO_MIME_TYPE);
        if (file_exists($filepath))
            $mt = explode('/', finfo_file($fi, $filepath));
        finfo_close($fi);
        return isset($mt[1]) ? $mt[0] == 'image' && in_array($mt[1], ['png', 'jpg', 'jpeg', 'gif']) : false;
    }

    public function textContainsUrl($text) {
        return !!preg_match('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i', $text);
    }

    public function dateFormat($date, $full = false, $isTimestamp = false) {
        $now = new \DateTime;
        $then = new \DateTime(($isTimestamp ? '@' : '') . $date);
        $diff = $now->diff($then); 
     
        $values = [
            'y' => ['gadiem', 'gada'],
            'm' => ['mēnešiem', 'mēneša'],
            'd' => ['dienām', 'dienas'],
            'h' => ['stundām', 'stundas'],
            'i' => ['minūtēm', 'minūtes'],
        ]; 
     
        foreach ($values as $key => &$value) {
            if ($diff->{$key}) {
                $time = $diff->{$key};
                $value = substr($time, -2) != 1 || substr($time, -2) == 11 ? $value[0] : $value[1];
                
                $value = $time . ' ' . $value;
            } else {
                unset($values[$key]);
            }
        } 
     
        if (!$full) $values = array_slice($values, 0, 1);
        return ($diff->invert || empty($values) ? 'pirms ' : 'pēc ') . (implode(' ', $values) ?: 'brīža');
    }

    /**
     * Returns a file size limit in bytes based on
     * the PHP upload_max_filesize and post_max_size
     *
     * @return float
     */
    public function getMaxUploadSize() {
        static $maxSize = -1;

        if ($maxSize < 0) {
            // Start with post_max_size.
            $maxSize = $this->parseFilesize(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $uploadMax = $this->parseFilesize(ini_get('upload_max_filesize'));
            if ($uploadMax > 0 && $uploadMax < $maxSize) {
                $maxSize = $uploadMax;
            }
        }
        return $maxSize;
    }

    /**
     * @see http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
     */
    protected function parseFilesize($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }

    function getHexBrightness($hex, $verbose = false, $compareAmount = 382) {
        $hex = ltrim($hex, '#');

        //break up the color in its RGB components
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));

        $brightness = $r + $g + $b;

        if (!$verbose) {
            return $brightness;
        }

        //do simple weighted average
        //
        //(This might be overly simplistic as different colors are perceived
        // differently. That is a green of 128 might be brighter than a red of 128.
        // But as long as it's just about picking a white or black text color...)
        if($brightness > $compareAmount){
            return 'bright';
        } else {
            return 'dark';
        }
    }
}
